[project]
name = "xair-api"
version = "2.4.1"
description = "Remote control Behringer X-Air | Midas MR mixers through OSC"
authors = [
    {name = "Onyx and Iris",email = "code@onyxandiris.online"}
]
license = {text = "MIT"}
readme = "README.md"
requires-python = ">=3.10"
dependencies = [
    "python-osc (>=1.9.3,<2.0.0)",
    "tomli (>=2.0.1,<3.0) ; python_version < '3.11'"
]

[tool.poetry.requires-plugins]
poethepoet = "^0.32.1"

[tool.poetry.group.dev.dependencies]
pytest = "^8.3.4"
pytest-randomly = "^3.16.0"
ruff = "^0.8.6"
tox = "^4.23.2"
virtualenv-pyenv = "^0.5.0"

[build-system]
requires = ["poetry-core>=2.0.0,<3.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.poe.tasks]
obs.script = "scripts:ex_obs"
sends.script = "scripts:ex_sends"
headamp.script = "scripts:ex_headamp"
test-xair.script = "scripts:test_xair"
test-x32.script = "scripts:test_x32"
test-all.script = "scripts:test_all"

[tool.tox]
legacy_tox_ini = """
    [tox]
    envlist = py310,py311,py312,py313

    [testenv]
    passenv = 
      TEST_MODULE
    setenv = VIRTUALENV_DISCOVERY=pyenv
    allowlist_externals = poetry
    commands_pre =
      poetry install --no-interaction --no-root
    commands =
      poetry run pytest tests/{env:TEST_MODULE}

    [testenv:obs]
    setenv = VIRTUALENV_DISCOVERY=pyenv
    allowlist_externals = poetry
    deps = obsws-python
    commands_pre =
      poetry install --no-interaction --no-root --without dev
    commands =
      poetry run python examples/xair_obs
"""

[tool.ruff]
exclude = [
    ".bzr",
    ".direnv",
    ".eggs",
    ".git",
    ".git-rewrite",
    ".hg",
    ".mypy_cache",
    ".nox",
    ".pants.d",
    ".pytype",
    ".ruff_cache",
    ".svn",
    ".tox",
    ".venv",
    "__pypackages__",
    "_build",
    "buck-out",
    "build",
    "dist",
    "node_modules",
    "venv",
]

# Same as Black.
line-length = 88
indent-width = 4

# Assume Python 3.10
target-version = "py310"

[tool.ruff.lint]
# Enable Pyflakes (`F`) and a subset of the pycodestyle (`E`)  codes by default.
# Unlike Flake8, Ruff doesn't enable pycodestyle warnings (`W`) or
# McCabe complexity (`C901`) by default.
select = ["E4", "E7", "E9", "F"]
ignore = []

# Allow fix for all enabled rules (when `--fix`) is provided.
fixable = ["ALL"]
unfixable = []

# Allow unused variables when underscore-prefixed.
dummy-variable-rgx = "^(_+|(_+[a-zA-Z0-9_]*[a-zA-Z0-9]+?))$"

[tool.ruff.format]
# Unlike Black, use single quotes for strings.
quote-style = "single"

# Like Black, indent with spaces, rather than tabs.
indent-style = "space"

# Like Black, respect magic trailing commas.
skip-magic-trailing-comma = false

# Like Black, automatically detect the appropriate line ending.
line-ending = "auto"

# Enable auto-formatting of code examples in docstrings. Markdown,
# reStructuredText code/literal blocks and doctests are all supported.
#
# This is currently disabled by default, but it is planned for this
# to be opt-out in the future.
docstring-code-format = false

# Set the line length limit used when formatting code snippets in
# docstrings.
#
# This only has an effect when the `docstring-code-format` setting is
# enabled.
docstring-code-line-length = "dynamic"

[tool.ruff.lint.mccabe]
max-complexity = 10

[tool.ruff.lint.per-file-ignores]
"__init__.py" = [
    "E402",
    "F401",
]
